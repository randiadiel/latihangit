<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
           html,body{
                margin: 0;
                padding: 0;
                width: 100%;
                height: 100vh;
                font-family: sans-serif;
                }
                nav{
                margin: 0;
                padding: 10px 50px;
                height: 50px;
                background: #333;
                display: flex;
                justify-content: space-between;
                align-items: center;
                position: sticky;
                }
                nav a{
                margin: 0;
                text-decoration: none;
                }
                nav ul{
                list-style-type: none;
                display: flex;
                }
                nav>ul a{
                text-decoration: none;
                margin: 10px;
                cursor: pointer;
                color: white;
                }
                section{
                margin: 0;
                padding: 0;
                width: 100%;
                height: 100%;
                background: url('https://codetheweb.blog/assets/img/posts/css-advanced-background-images/mountains.jpg');
                background-size: cover;
                text-align: center;
                }
                section h1{
                color: #f97;
                }
                section div{
                margin: 0;
                color: white;
                font-family: sans-serif;
                left: 50%;
                top: 50%;
                width: 100%;
                position: relative;
                transform: translate(-50%,-50%);
                display: flex;
                flex-direction: column;
                }
                section>div h1 , p{
                color: white;
                margin: 0;
                }
                section>div h1{
                font-size: 200%;
                margin-bottom: 10px;
                }
                section>div p{
                font-size: 150%;
                }
                #alertMess {
                display: block;
                padding-left: 10px;
                padding-right: 10px;

                background-color: #ff4949;
                border-radius: 4px;

                color: #ffffff;
                font-family: sans-serif;
                font-size: 16px;
                line-height: 44px;
                }
        </style>
        <script src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
        <!-- Load React DOM-->
        <script src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
        <!-- Load Babel Compiler -->
        <script src="https://unpkg.com/babel-standalone@6.15.0/babel.min.js"></script>
    </head>
    <body>
            <section>
                <nav>
                <h1 id="navLogo"></h1>
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="{{ URL('/passports') }}">View Appointment</a></li>
                    <li><a href="{{ URL('/passports/create') }}">Create Appointment</a></li>
                </ul>
                </nav>
                <div>
                <h1 id="heroCapt"></h1>
                <p id="clock"></p>
                </div>
                <div id="alertMess"></div>
            </section>

        <!-- <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div> -->
        <script type="text/babel">

                const name ='Randi Adiel Gianufian';
                ReactDOM.render(<a href="#"><h1> {name} </h1></a>, document.getElementById('navLogo'));
                ReactDOM.render(<h1> Hello, {name} ! </h1>, document.getElementById('heroCapt'));


                function clock(){
                    const clockElement = (
                    <div>
                        <p> It Is {new Date().toLocaleTimeString()}.</p>
                    </div>
                    );
                    ReactDOM.render(clockElement, document.getElementById('clock'));
                }
                setInterval(clock,1000);


                function Alert(props) {
                    return <div>{ props.name }</div>;
                }


                function alerting(){
                    const alertComponent = <Alert name="The Button Have Been Clicked !" />;
                    ReactDOM.render(
                    alertComponent,
                    document.getElementById('alertMess')
                    );
                }
            </script>
    </body>
</html>
